#include "userthread.h"
#include "synch.h"
#include "system.h"

//----------------------------------------------------------------------
// ThreadInfo
//----------------------------------------------------------------------

ThreadInfo::ThreadInfo(Thread *a_thread, SpaceId id, int a_priority = 10)
{
    thread = a_thread;
    SpID = id;
    priority = a_priority;
    renice = ticks = returnValue = 0;
    next = prev = 0;
    exit = new Semaphore("exit thread", 0);
}

ThreadInfo::~ThreadInfo()
{
    delete exit;
    if (prev) prev->next = next;
    if (next) next->prev = prev;
}

//----------------------------------------------------------------------
// OpenFileId OpenFileTable::Insert(OpenFile *of)

OpenFileId OpenFileTable::Insert(OpenFile *of)
{
    for (int i=2; i < size; i ++)
      if (table[i] == 0) {
	table[i] = of;
	return (OpenFileId) i;
      }
    return (OpenFileId) -1;
}

bool OpenFileTable::Remove(OpenFileId id)
{
  int i = (int) id;
  
  if (table[i] != 0) {
    delete table[i];			// zavre soubor
    table[i] = 0;
    return true;
  }
  return false;
}
//-----------------------------------------------------------------------
// konstruktor a destruktor struktur KernelSemList

KernelSemItem::KernelSemItem( int _key, int value )
{
    key = _key;
    used = 1;
    sem = new Semaphore( "User Semaphore", value );
    // next a prev nastavi volajici
    next = prev = 0;
}

KernelSemItem::~KernelSemItem()
{
    delete sem;
    if ( next ) next -> prev = prev;
    if ( prev ) prev -> next = next;
    if ( this == semList ) semList = next;
}

//-----------------------------------------------------------------------
// ThreadSemTable udrzuje

bool ThreadSemTable::Create( int key, int value )	// vytvari novy semafor pro thread
{

    if ( SemFindKey( key ) != 0 ) return false;
	    // nemuze byt vytvoreno vice semaforu se stejnym klicem

    SemEntry * p = new SemEntry;	// zacleneni do struktur
    p -> next = first;
    p -> prev = 0;
    
    if ( first ) 
	first -> prev = p;
    first = p;
    
    p -> item = new KernelSemItem( key, value );	// zaradime novy
    p -> item -> next = semList;			// semafor do globalniho
    if ( semList ) semList -> prev = p -> item;		// seznamu
    semList = p -> item;
    return true;    
}

bool ThreadSemTable::Attach( int key )	// zadost o vyuzivani semaforu
{					// vytvoreneho s klicem key
    KernelSemItem * p;

    if ( ( p = SemFindKey( key ) ) != 0 )
    {
        SemEntry * se = new SemEntry;	// zacleneni do struktur
	se -> next = first;
	se -> prev = 0;
    
	if ( first ) 
	    first -> prev = se;
	first = se;
    
	se -> item = p;
	se -> item -> used++;
	return true;
    }
    
    return false;		// semafor nebyl nalezen
}

void ThreadSemTable::Detach( int key )	// odpoji se od semaforu
{
    SemEntry * p;
    
    if ( ( p = SemFindEntry( key ) ) != 0 )
	Detach(p);
}

void ThreadSemTable::P( int key )
{
    SemEntry * p;
    
    if ( ( p = SemFindEntry( key ) ) != 0 )
	p->item->sem->P();
}

void ThreadSemTable::V( int key )
{
    SemEntry * p;
    
    if ( ( p = SemFindEntry( key ) ) != 0 )
	p->item->sem->V();
}

bool ThreadSemTable::Detach( SemEntry * p)
{
    if ( --p -> item -> used == 0 )
    {
        delete p -> item;		// pokud jsem byl posledni, smazat
    }
    if (p->prev) p->prev->next = p->next;
    if (p->next) p->next->prev = p->prev;
    if (p == first) first = p->next;
    delete p;	

    return (bool) first;		// vrati jestli je jeste neco v seznamu
}

void ThreadSemTable::DetachAll()
{
    if (first) while ( Detach(first) ) ;
}

KernelSemItem * ThreadSemTable::SemFindKey( int key ) // vyhledani podle klice v semaforech
{					// v celem systemu
    KernelSemItem * p = semList;
    
    while ( p ) 
	if ( p -> key == key ) break;
	else p = p -> next;
    
    return p;
}

ThreadSemTable::SemEntry * ThreadSemTable::SemFindEntry( int key ) // vyhledani podle klice v semaforech
{					// jednoho threadu
    SemEntry * p = first;
    
    while ( p ) 
	if ( p -> item -> key == key ) break;
	else p = p -> next;
    
    return p;
}
