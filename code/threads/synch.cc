// synch.cc 
//	Routines for synchronizing threads.  Three kinds of
//	synchronization routines are defined here: semaphores, locks 
//   	and condition variables (the implementation of the last two
//	are left to the reader).
//
// Any implementation of a synchronization routine needs some
// primitive atomic operation.  We assume Nachos is running on
// a uniprocessor, and thus atomicity can be provided by
// turning off interrupts.  While interrupts are disabled, no
// context switch can occur, and thus the current thread is guaranteed
// to hold the CPU throughout, until interrupts are reenabled.
//
// Because some of these routines might be called with interrupts
// already disabled (Semaphore::V for one), instead of turning
// on interrupts at the end of the atomic operation, we always simply
// re-set the interrupt state back to its original value (whether
// that be disabled or enabled).
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "synch.h"
#include "system.h"

//----------------------------------------------------------------------
// Semaphore::Semaphore
// 	Initialize a semaphore, so that it can be used for synchronization.
//
//	"debugName" is an arbitrary name, useful for debugging.
//	"initialValue" is the initial value of the semaphore.
//----------------------------------------------------------------------

Semaphore::Semaphore(char* debugName, int initialValue)
{
    name = debugName;
    value = initialValue;
    queue = new List;
}

//----------------------------------------------------------------------
// Semaphore::Semaphore
// 	De-allocate semaphore, when no longer needed.  Assume no one
//	is still waiting on the semaphore!
//----------------------------------------------------------------------

Semaphore::~Semaphore()
{
    delete queue;
}

//----------------------------------------------------------------------
// Semaphore::P
// 	Wait until semaphore value > 0, then decrement.  Checking the
//	value and decrementing must be done atomically, so we
//	need to disable interrupts before checking the value.
//
//	Note that Thread::Sleep assumes that interrupts are disabled
//	when it is called.
//----------------------------------------------------------------------

void
Semaphore::P()
{
    IntStatus oldLevel = interrupt->SetLevel(IntOff);	// disable interrupts
    
    while (value == 0) { 			// semaphore not available
	queue->Append((void *)currentThread);	// so go to sleep
	DEBUG('s', "Thread %s is going to sleep in semaphore %s\n", currentThread->getName(), name);
	currentThread->Sleep();
    } 
    value--; 					// semaphore available, 
						// consume its value

    DEBUG('s', "Thread %s set down semaphore %s\n", currentThread->getName(), name);
    (void) interrupt->SetLevel(oldLevel);	// re-enable interrupts
}

//----------------------------------------------------------------------
// Semaphore::V
// 	Increment semaphore value, waking up a waiter if necessary.
//	As with P(), this operation must be atomic, so we need to disable
//	interrupts.  Scheduler::ReadyToRun() assumes that threads
//	are disabled when it is called.
//----------------------------------------------------------------------

void
Semaphore::V()
{
    Thread *thread;
    IntStatus oldLevel = interrupt->SetLevel(IntOff);

    thread = (Thread *)queue->Remove();
    if (thread != NULL) {	   // make thread ready, consuming the V immediately
	scheduler->ReadyToRun(thread);
	DEBUG('s', "Waking up thread %s sleeping in semaphore %s\n", thread->getName(), name);
    }
    value++;
    DEBUG('s', "Thread %s set up semaphore %s\n", currentThread->getName(), name);
    (void) interrupt->SetLevel(oldLevel);
}

// Dummy functions -- so we can compile our later assignments 
// Note -- without a correct implementation of Condition::Wait(), 
// the test case in the network assignment won't work!
//
// pokud je definovany SEMLOCK, tak se prekladaji zamky 
// implementovane pomoci semaforu, jinak jsou implementovane 
// nad frontou ready procesu
// jako identifikace Threadu, ktery zamek zamkl, slouzi promenna 
// owner a tato promenna je pouzita v metode isHeldByCurrentThread(),
// ktera oznamuje zda jsme vlastnikem zamku ci nikoli

Lock::Lock(char* debugName)
{
// begin of "added by M.K. & J.K."

    name = debugName;
    owner = NULL;
#ifndef SEMLOCK
    locked = false;
    queue = new List;
#else
    sem = new Semaphore("LockSem", 1);
#endif

// end of "added by M.K. & J.K."
}

// pokud rusime zamek jehoz fronta cekajicich procesu neni prazdna
// povazujeme to za chybu a proto je pouzit ASSERT
Lock::~Lock()
{
// begin of "added by M.K. & J.K."

    DEBUG('t', "Deleting lock");
#ifndef SEMLOCK
    ASSERT(queue->IsEmpty());
    delete queue;
#else
    delete sem;
#endif

// end of "added by M.K. & J.K."
}

// pokud se snazi Thread zamknout jiz jednou jim zamknuty zamek
// povazujeme to za chybu a proto ASSERT
// kdyz Thread zamek nevlastni a nepodari se mu ho zamknout, protoze
// ho uz zamkl nekdo jiny, Thread jde spat
void Lock::Acquire()
{
// begin of "added by M.K. & J.K."

    IntStatus oldLevel = interrupt->SetLevel(IntOff);

#ifndef SEMLOCK
    if (locked) {
	ASSERT(isHeldByCurrentThread() == false);
	queue->Append((void*) currentThread);
	DEBUG('s', "Thread %s is going to sleep in lock %s\n", name, currentThread->getName());
	currentThread->Sleep();
    }
    locked = true;
#else
    ASSERT(isHeldByCurrentThread() == false);
    sem->P();
#endif
    owner = currentThread;
    DEBUG('s', "Thread %s has locked lock %s\n", currentThread->getName(), name);
    (void) interrupt->SetLevel(oldLevel);

// end of "added by M.K. & J.K."
}

// Kdyz odemykame nezamceny zamek, je owner nastaven na NULL, a proto 
// nam nepatri. Kdyz nam nepatri zamek, nemame ho co odemykat a nasleduje
// ASSERT.
void Lock::Release()
{
// begin of "added by M.K. & J.K."

    IntStatus oldLevel = interrupt->SetLevel(IntOff);
    ASSERT(isHeldByCurrentThread());	// zamek nam nepatri
#ifndef SEMLOCK
    
    Thread *p;
    if ( ( p = (Thread*) queue->Remove()) ) {
	owner = p;
	scheduler->ReadyToRun(p);
	DEBUG('s', "waking up thread %s sleeping in lock %s\n", p->getName(), name);
    }
    else {
	owner = NULL;
	locked = false;		// odemkni
    }
#else
    owner = NULL;
    sem->V();			// odemkni
#endif
    
    DEBUG('s', "lock %s unlocked by thread\n", name, currentThread->getName());
    (void) interrupt->SetLevel(oldLevel);

// end of "added by M.K. & J.K."
}

bool Lock::isHeldByCurrentThread()
{
// begin of "added by M.K. & J.K."

    return (currentThread == owner);

// end of "added by M.K. & J.K."
}

// potrebujeme frontu cekajicich Threadu
// pouzivame MESA konvenci - thread, ktery posila signal (broadcast), 
// pouze naplanuje zablokovany(e) proces(y) a ti musi cekat az opusti
// volajici KS
// testovani zda predavame do Waitu zamek, ktery byl zamcen volajicim 
// Threadem, se provadi uvnitr metody Lock::Release()
// pokod volame Signal nebo Broadcast s jinym nez vlastnim zamkem je
// vyvolan ASSERT
// zamek ktery nikdo nedrzi neni nas
Condition::Condition(char* debugName)
{
    name = debugName;
    queue = new List;
}
Condition::~Condition()
{
    ASSERT(queue->IsEmpty());
    delete queue;
}

void Condition::Wait(Lock* conditionLock)
{
    IntStatus oldLevel = interrupt->SetLevel(IntOff);
    DEBUG('s', "Thread %s is going to wait on CondVar %s\n", currentThread->getName(), name);
    queue->Append((void*) currentThread);
    conditionLock->Release();
    currentThread->Sleep();			// musi byt zakazane preruseni
    conditionLock->Acquire();
    DEBUG('s', "Thread %s is leaving Wait in CondVar %s\n", currentThread->getName(), name);
    (void) interrupt->SetLevel(oldLevel);
}
void Condition::Signal(Lock* conditionLock)
{
    IntStatus oldLevel = interrupt->SetLevel(IntOff);
    ASSERT(conditionLock->isHeldByCurrentThread());
    DEBUG('s', "Thread %s is sending Signal to CondVar %s\n", currentThread->getName(), name);
    
    Thread *p;
    
    if ( (p = (Thread*) queue->Remove()) )
	scheduler->ReadyToRun(p);
    (void) interrupt->SetLevel(oldLevel);
}
void Condition::Broadcast(Lock* conditionLock)
{
    IntStatus oldLevel = interrupt->SetLevel(IntOff);
    ASSERT(conditionLock->isHeldByCurrentThread());
    DEBUG('s', "Thread %s is sending Broadcast to CondVar %s\n", currentThread->getName(), name);

    Thread *p;
    
    while ( (p = (Thread*) queue->Remove()) )
	scheduler->ReadyToRun(p);
    (void) interrupt->SetLevel(oldLevel);
}
