/*
    V techto mistech jsou definovany struktury, potrebne k provozu
    uzivatelskych threadu.
    ThreadInfo - udrzuje Threadu seznam jeho deti a pro ne jejich
	identifikacni cisla, prioritu, navratovou hodnotu a dobu
	kdy byli naposledy na procesoru
    
    OpenFileId - udrzuje pro Thread seznam otevrenych souboru, ve
	svem destruktoru, vsechny otevrene soubory zavre
	
    UserSem - udrzuje informace o jednom uzivatelskem semaforu. Eviduje klic 
	semaforu, kterym je jednoznacne identifikovan v celem systemu,
	identifikator semaforu, SpID vlastnika a pocet
	procesu, ktere k semaforu chteji pristupovat
*/
#ifndef USERTHREAD_H
#define USERTHREAD_H

#define MAXOPENFILES	14

#include "openfile.h"

typedef int SpaceId;
typedef int OpenFileId;

class Thread;
class Semaphore;

class ThreadInfo {
 public:
    ThreadInfo(Thread *a_thread, SpaceId id, int a_priority = 10);
    ~ThreadInfo();

    int getPriority() {
      return priority + renice;
    }
    
    Thread *thread;
    SpaceId SpID;
    int priority, renice, ticks, returnValue;
    ThreadInfo *next, *prev;
    Semaphore *exit;
};

class OpenFileTable {
 public:
  OpenFileTable(int maxSize = MAXOPENFILES ) {
    table = new (OpenFile *)[size=maxSize];
    for (int i=2; i < size; table[i++]=0) ;
  }
  ~OpenFileTable() {
    for (int i=2; i < size; i ++)	// 0, 1 - je standartni vstup a vystup
      if (table[i]) delete table[i];	// zavri otevrene soubory
    delete[] table;
  }
  OpenFileId Insert(OpenFile *of);	// priradi OpenFileId k oteviranemu souboru
  bool Remove(OpenFileId id);
  OpenFile *getOpenFile(OpenFileId id) {// vrati OpenFile k file descriptoru
    return table[(int) id];
  }
  
private:
  int size;
  OpenFile **table;
};

//struct UserSem {	
struct KernelSemItem {	// struktura udrzujici informace o vsech semaforech
    int key;		// v systemu
    int used;		// kolika thready je semafor pouzivan
    Semaphore *sem;
    KernelSemItem *next, *prev;
    KernelSemItem( int _key, int value );
    ~KernelSemItem();
};

class ThreadSemTable {	// seznam semaforu pouzivanych jednim threadem

struct SemEntry {
    KernelSemItem * item;
    SemEntry * prev, * next;
};

public:

    ThreadSemTable()
    {
	first = 0;
    }
    ~ThreadSemTable()
    {
	DetachAll();
    }
    
    bool Create( int key, int value );	// vytvori novy semafor
    bool Attach( int key );		// pokusi se pripojit k semaforu
    void Detach( int key );		// odpoji se od semaforu s klicem key
    void P( int key );
    void V( int key );
    
private:
    bool Detach( SemEntry * );		// odpoji se od semaforu
    void DetachAll();			// odpoji vsechny semafory
    KernelSemItem * SemFindKey( int key );
    SemEntry * SemFindEntry( int key );

    SemEntry * first;
};

#endif
