// exception.cc 
//	Entry point into the Nachos kernel from user programs.
//	There are two kinds of things that can cause control to
//	transfer back to here from user code:
//
//	syscall -- The user code explicitly requests to call a procedure
//	in the Nachos kernel.  Right now, the only function we support is
//	"Halt".
//
//	exceptions -- The user code does something that the CPU can't handle.
//	For instance, accessing memory that doesn't exist, arithmetic errors,
//	etc.  
//
//	Interrupts (which can also cause control to transfer from user
//	code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"
#include "filesys.h"
#include "openfile.h"
#include "addrspace.h"

#define INIT_FILE_SIZE	1024

// pridano MK &JK
#ifdef USER_PROGRAM
static OpenFileId getOpenFileId(OpenFile *of)
{
  return currentThread->ofTable->Insert(of);
}
static OpenFile *getOpenFile(OpenFileId id)
{
  return currentThread->ofTable->getOpenFile(id);
}
static bool CloseOpenFile(OpenFileId id)
{
  return currentThread->ofTable->Remove(id);
}
#endif

// funkce pro prevod adresy v uzivatelskem programu na adresu
// pouzitelnou v kernelu a nazpatek
int GetPhysAddrInKernel(int virtualAddr, int writing);
char *UserToKernelString(char *source);
char *UserToKernel(char *source, int length);
void KernelToUser(char *dest, char *source, int length);

// konec KM &JK

//----------------------------------------------------------------------
// ExceptionHandler
// 	Entry point into the Nachos kernel.  Called when a user program
//	is executing, and either does a syscall, or generates an addressing
//	or arithmetic exception.
//
// 	For system calls, the following is the calling convention:
//
// 	system call code -- r2
//		arg1 -- r4
//		arg2 -- r5
//		arg3 -- r6
//		arg4 -- r7
//
//	The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//	"which" is the kind of exception.  The list of possible exceptions 
//	are in machine.h.
//----------------------------------------------------------------------

// vytvorili M.K. & J.K.

void ExecProcess(int)
{
  DEBUG('4', "Starting process %s\n", currentThread->getName());

  currentThread->space->InitRegisters();
  currentThread->space->RestoreState(); // load page table register

  machine->Run();                       // jump to the user progam
  ASSERT(FALSE);                        // machine->Run never returns;
                                        // the address space exits
                                        // by doing the syscall "exit"
}

void ExecThread( int function )
{
    DEBUG( '4', "Starting process %s by fork\n", currentThread->getName() );
    
    currentThread -> space -> InitRegisters( function );
    currentThread -> space -> RestoreState();
    
    machine -> Run();
    ASSERT( false );
}

void ExceptionHandler(ExceptionType which)
{
    int type = machine->ReadRegister(2);
    
    switch (which) {
    case NoException:
	break;
    case SyscallException:

      switch (type) {
	case SC_Halt:
	    DEBUG('m', "Syscall: Shutdown, initiated by user program\n");
	    interrupt->Halt();
	  break;

	case SC_Exit:
	  {
	    int retVal = machine->ReadRegister(4);
	    DEBUG('m', "Syscall: Exiting user thread %s with exit code %i\n", currentThread->getName(), retVal);
	    
	    ThreadInfo *p;	// cekej na vsechny svoje potomky az se dokonci
	    while ((p = currentThread->child) != 0) {
		p->exit->P();
		currentThread->child = p->next;
		if ( p -> next ) currentThread->child->prev = 0;
		delete p;
	    }
	    
	    currentThread->info->returnValue = retVal;
	    
	    if (--threadCount > 0) {			// neni to posledni thread
		currentThread->info->exit->V();
		currentThread->Finish();
	    }
	    else					// posledni thread
		interrupt->Halt();
	    
	    ASSERT(FALSE);		// sem by se nikdy nemel dostat
	  }
	  break;

	case SC_Exec:
	  {
	    char *name = UserToKernelString((char *) machine->ReadRegister(4));
	    DEBUG('m', "Syscall: Executing %s\n", name);
	    
	    OpenFile *executable = fileSystem->Open(name);
	    if (executable == NULL) {
	      machine->WriteRegister(2, -1);
	      delete name;
	      break;
	    }
	    AddrSpace *sp = new AddrSpace(executable);
	    delete executable;
	    
	    SpaceId SpID = -1;
	    
	    if ( sp -> allocated )
	    {
		Thread *t = new Thread(name);

		t->info = new ThreadInfo(t, SpID = nextSpID++);
		t->space = sp;
		t->info->next = currentThread->child;
		if (currentThread->child)
		    currentThread->child->prev = t->info;
		currentThread->child = t->info;
		
		threadCount++;
		t->Fork(ExecProcess, 0);
	    }
	    else 
	    {
		printf("neni dost pameti\n");
	        delete sp;
	    }
	    delete name;

	    machine->WriteRegister(2, SpID);
	  }
	  break;

	case SC_Join:
	  {
	    SpaceId SpID = machine->ReadRegister(4);
	    DEBUG('m', "Syscall: attempting to join with process %i\n", (int) SpID);
	    ThreadInfo *p = currentThread->child;
	    while (p != 0) {
		if (p->SpID == SpID) {		// thread na kterej cekam
		    p->exit->P();

		    DEBUG('m', "Syscall: proces %i joined with process %i\n",
			    (int) currentThread->info->SpID, (int) SpID);
		    int retVal = p->returnValue;
		    if (p == currentThread->child)
			currentThread->child = p->next;
		    delete p;
		    machine->WriteRegister(2, retVal);
		    return;
		}
		p = p->next;
	    }
	    // nenasel jsem SpID mezi svymi detmi, tak vrati, ze je vse OK
	    machine->WriteRegister(2, 0);
	  }
	  break;

	case SC_Create:
	  {
	    // preneseme si jmeno tvoreneho souboru z uzivatelskeho programu
	    // do jadra
	    char *name = UserToKernelString(
			(char *)machine -> ReadRegister(4) );
	    DEBUG('m', "Syscall: Creating file %s\n", name);
	    // INIT_FILE_SIZE je definovano na zacatku tohoto souboru,
	    // znaci nami zvolenou maximalni velikost souboru v pripade pouziti
	    // souboroveho systemu urceneho kompilaci s -DFILESYS
	    fileSystem -> Create( name, INIT_FILE_SIZE );
	    delete name;	// name bylo dynamicky alokovane
	  }
	  break;
	  
	case SC_Open:
	  {
	    // preneseme jmeno oteviraneho souboru do oblasti jadra
	    char * name = UserToKernelString(
			(char *)machine -> ReadRegister(4) );
	    OpenFile * of;
	    OpenFileId fid;
	    DEBUG( 'm', "Syscall: Opening file %s\n", name );
	    of = fileSystem -> Open( name );
	    // fileSystem -> Open() v pripade neuspechu vrati NULL
	    if ( !of ) fid = -1;	// soubor nebyl otevren !
	    else fid = getOpenFileId( of );
	    delete name;	// uvolnime pouzite prostredky
	    machine -> WriteRegister( 2, fid );
	  }
	  break;
	  
	case SC_Read:
	  {
	    OpenFileId fd = machine->ReadRegister(6);
	    DEBUG( 'm', "Syscall: Reading from file descriptor %d\n", fd );
	    int count = -1;
	    // musime osetrit pripadne programatorovo neosetreni chyby 
	    // po neuspesnem provedeni syscallu Open()
	    if ( fd != -1) {
	      int size = machine -> ReadRegister( 5 );
	      char * buf1 = (char *)(machine -> ReadRegister( 4 ) );  // kernel
	      char * buf2 = new char[size];				// user
		
	      switch (fd) {
	      case ConsoleInput:
		for ( count = 0 ; count < size ; count ++ )
		  buf2[ count ] = synchCon -> GetChar();
		break;
	      case ConsoleOutput:
		// z vystupu nebudou nactena zadna data
		break;
	      default:
		{
		  OpenFile *of = getOpenFile(fd);
		  count = of->Read( buf2, size );
		}
		break;
	      }
		
	      KernelToUser(buf1, buf2, count);
	      delete buf2;
	      machine -> WriteRegister( 2, count );
		
	    } // if
	  } // case
	  break;

	case SC_Write:
	  {
	    OpenFileId fd = machine -> ReadRegister( 6 );
	    DEBUG( 'm', "Syscall: Writing to file descriptor %d\n", fd );
	    int count = -1;
	    // opet kontrola ( pro jistotu... )
	    if ( fd != -1 )
	    {
	        int size = machine -> ReadRegister( 5 );
	        char * buf = UserToKernel((char *)(machine -> ReadRegister( 4 )), size);
	        if ( !buf ) {
	            machine -> WriteRegister( 2, - 1 );
		    break ;		// nelze cist z asresy 0x00 v pameti!
		}
	        switch (fd) {
	        case ConsoleInput:
		    // cist ze standardniho vystupu neni mozne
		  break;
	        case ConsoleOutput:
		  {
		    for (count = 0; count < size; count ++)
			synchCon->PutChar(buf[count]);
		  }
		  break;
	        default:
		  {
		    OpenFile *of = getOpenFile(fd);
		    count = of->Write( buf, size );
		  }
		  break;
	        }

	        delete buf;
	    }
	    // v syscall.h je poznamka o zmenene semantice syscallu Write
	    machine -> WriteRegister( 2, count );
	  }
	  break;

	case SC_Close:
	  {
	    OpenFileId fd = machine -> ReadRegister(4);
	    DEBUG('m', "Syscall: Closing file descriptor %i\n", fd);
	    if ( fd != -1 && fd != 0 && fd != 1 )
		CloseOpenFile(fd);	// uzavreni souboru
	  }
	  break;

	case SC_Fork:
	  {
	      AddrSpace * sp = new AddrSpace( currentThread -> space );
	      
	      SpaceId SpID = -1;
	      
	      if ( sp -> allocated )
	      {
	          Thread * t = new Thread( currentThread -> getName() );
		  t -> info = new ThreadInfo( t, SpID = nextSpID++ );
		  t -> space = sp;
		  t -> info -> next = currentThread -> child;
		  if ( currentThread -> child )
		      currentThread -> child -> prev = t -> info;
		  currentThread -> child = t -> info;
		  
		  threadCount++;
		  t -> Fork( ExecThread, machine -> ReadRegister( 4 ) );
		}
		else
		{
		    printf( "Not enough memory to fork this thread" );
		    SpID = -1;
		    delete sp;
		}
	      machine -> WriteRegister( 2, SpID );
	    }

	  break;

	case SC_Yield:
	  currentThread->Yield();
	  break;
	  
	case SC_Renice:
	  {
	    int renice = machine->ReadRegister(4);
	    currentThread->info->priority += renice;
	    if (currentThread->info->priority < 0)
		currentThread->info->priority = 0;
	    DEBUG('m', "Syscall: Setting priority of process \"%s\" to %i\n",
		currentThread->getName(), currentThread->info->getPriority());
	    machine->WriteRegister(2, currentThread->info->priority);
	  }
	  break;
	
	case SC_Length:
	  {
	    OpenFileId fd = machine -> ReadRegister(4);
	    DEBUG( 'm', "Syscall: Asking for length of file descriptor %d\n", fd );
	    int flength = -1;
	    if ( fd != -1 )	// kontrola
	    {
	      if ( fd == ConsoleInput || fd == ConsoleOutput ) flength = 0;
	      else {
		OpenFile * of = getOpenFile( fd );
		flength = of -> Length();
	      }
	    }
	    machine -> WriteRegister( 2, flength );
	  }
	  break;

	case SC_Remove:
	  {
	    char * name = UserToKernelString( 
			 (char *)machine -> ReadRegister(4) );
	    DEBUG( 'm', "Syscall: Removing file %s\n", name );
	    bool succes = fileSystem->Remove( name );
	    delete name;
	    machine -> WriteRegister( 2, (int)succes );
	  }
	  break;
	case SC_SemGet:
	  {
	    int key = machine->ReadRegister( 4 );
	    int param = machine->ReadRegister( 5 );
	    int ret;
	    if (param < 0)
		ret = (int) currentThread->semTable->Attach(key);
	    else
		ret = (int) currentThread->semTable->Create(key, param);
	    machine->WriteRegister( 2, ret);
	  }
	  break;		// currentThread->info
	case SC_SemCtl:
	  {
	    int key = machine->ReadRegister( 4 );
	    SemAction action = (SemAction) machine->ReadRegister( 5 );
	    switch (action) {
	      case SemDetach:
	        currentThread->semTable->Detach(key);
		break;
	      case SemP:
	        currentThread->semTable->P(key);
	        break;
	      case SemV:
	        currentThread->semTable->V(key);
	        break;
	    }
	  }
	  break;
	}
	break;	
    
case PageFaultException:
	DEBUG('m', "PageFaultException\n");
	printf("PageFaultException\n");
	currentThread->Finish();
    break;
    
    case ReadOnlyException:
	DEBUG('m', "ReadOnlyException\n");
	printf("ReadOnlyException\n");
	currentThread->Finish();
    break;
    
    case BusErrorException:
	DEBUG('m', "BussErrorException\n");
	printf("BussErrorException\n");
	currentThread->Finish();
    break;
    
    case AddressErrorException:
	DEBUG('m', "AddressErrorException\n");
	printf("AddressErrorException\n");
	currentThread->Finish();
    break;
    
    case OverflowException:
	DEBUG('m', "OverflowException\n");
	printf("OverflowException\n");
	currentThread->Finish();
    break;
    
    case IllegalInstrException:
	DEBUG('m', "IllegalInstrException\n");
	printf("IllegalInstrException\n");
	currentThread->Finish();
    break;
    
    case NumExceptionTypes:
	DEBUG('m', "NumExceptionTypes\n");
	printf("NumExceptionTypes\n");
	currentThread->Finish();
    break;
    }
}

//--------------------------------------------------------------------------
// GetPhysAddrInKernel

int GetPhysAddrInKernel(int virtualAddr, int writing)
{
  ExceptionType exception;
  int phyAddr;

  exception=machine->Translate(virtualAddr, &phyAddr, 1, writing);
  if(exception==PageFaultException){
    //UpdateTLB(virtualAddr);
    exception=machine->Translate(virtualAddr, &phyAddr, 1, writing);
  }
  if(exception!=NoException){
    ExceptionHandler(exception);
    return 0;
  }
  return phyAddr;
}

//--------------------------------------------------------------------------
// UserToKernelString
//	Kopiruje string z uzivatelskeho virtualniho prostoru do struktury
//	jadra, predpoklada ukonceni retezce nulovym znakem

char *UserToKernelString(char *source)
{
  int i=0;
  char *temp1, *temp2;
  int phyAddr;
  char *dest;

  if(!source)
    return 0;

  temp1=source;
  while(1){
    phyAddr=GetPhysAddrInKernel((int)temp1++, FALSE);
    if(!phyAddr)
      return 0;
    if(!machine->mainMemory[phyAddr])
     break;
    i++;
  }
/*  if(i==0)
    return 0;*/
  temp2=dest=new char[i+1];
  while((*temp2++=machine->mainMemory[GetPhysAddrInKernel((int)source++, FALSE)]) != 0) ;
  return dest;
}

//--------------------------------------------------------------------------
// UserToKernel
//	Kopiruje buffer delky length z uzivatelskeho adresoveho prostoru
//	do datove struktury v oblasti jadra

char *UserToKernel(char *source, int length)
{
  int current=0;
//  int phyAddr;
  char *dest;
  
  if(!source)
    return (int)0;
  dest=new char[length];
  while(current<length){
    machine->ReadMem((int)source++,1,(int *)&dest[current++]);
 //   phyAddr=GetPhysAddrInKernel((int)source++, FALSE);
  //  if(!phyAddr)
   //   return 0;
  //  dest[current++]=machine->mainMemory[phyAddr];
  }
  return dest;
}

//--------------------------------------------------------------------------
// KernelToUser
//	Kopiruje buffer o delce length z oblasti jadra do uzivatelkseho
//	prostoru

void KernelToUser(char *dest, char *source, int length)
{
  int current=0;
//  int phyAddr;
  
  while(current<length){
        machine->WriteMem((int)dest++,1,(int)source[current++]);
  //  phyAddr=GetPhysAddrInKernel((int)dest++, TRUE);
    // dest won't get deleted if address is invalid
   // machine->mainMemory[phyAddr]=source[current++];
  }
}

// konec M.K. & J.K.












