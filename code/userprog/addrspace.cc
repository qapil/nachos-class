// addrspace.cc 
//	Routines to manage address spaces (executing user programs).
//
//	In order to run a user program, you must:
//
//	1. link with the -N -T 0 option 
//	2. run coff2noff to convert the object file to Nachos format
//		(Nachos object code format is essentially just a simpler
//		version of the UNIX executable object code format)
//	3. load the NOFF file into the Nachos file system
//		(if you haven't implemented the file system yet, you
//		don't need to do this last step)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "addrspace.h"
#include "noff.h"

// pridano JK & MK
// Memory Management
// trida MemManager udrzuje strukturu volnych a obsazenych bloku

int MemManager::Allocate( int count, int ident )
{
    bool succes = false;
    int start, block, i;

    start = block = 0;

    // hledam dostatecne veliky souvisly blok dat
    while ( !succes && start < NumPhysPages )
    {
	for ( i = 0 ; (i < count) && (start + i < NumPhysPages ); i++ )
	{
	 if ( bitMap -> Test( start + i ) )
	      break;
	}
	
	if ( i < count ) start += i + 1;
	else 
	{
	    for ( i = 0 ; i < count ; i++ )
		bitMap -> Mark( start + i );
	    succes = true;
	}
    }
    
    if ( count != i ) start = -1;	// nepovedlo se alokovat dost pameti
    else
	{
	    MemoryEntry * p = new MemoryEntry;
	    p -> startPage = start;
	    p -> count = count;
	    p -> id = ident;
	    p -> next = usedMem;
	    usedMem = p;
	}
	
    return start;
}

void MemManager::Deallocate( int ident )
{
    MemoryEntry * p, * n, * d;
    
    p = 0;		// p je predchozi struktura
    n = usedMem;	// n je kontrolovana struktura
    
    while ( n -> id != ident )
    {
	p = n;
	n = n -> next;
	ASSERT( n );
    }
    
    d = n;		// d drzi blok k destrukci
    if ( p ) p = n -> next;
    else usedMem = n -> next;
    
    for ( int i = 0 ; i < d -> count ; i++ )
	bitMap -> Clear( d -> startPage + i );

    delete d;
}

// konec JK & MK

//----------------------------------------------------------------------
// SwapHeader
// 	Do little endian to big endian conversion on the bytes in the 
//	object file header, in case the file was generated on a little
//	endian machine, and we're now running on a big endian machine.
//----------------------------------------------------------------------

static void 
SwapHeader (NoffHeader *noffH)
{
	noffH->noffMagic = WordToHost(noffH->noffMagic);
	noffH->code.size = WordToHost(noffH->code.size);
	noffH->code.virtualAddr = WordToHost(noffH->code.virtualAddr);
	noffH->code.inFileAddr = WordToHost(noffH->code.inFileAddr);
	noffH->initData.size = WordToHost(noffH->initData.size);
	noffH->initData.virtualAddr = WordToHost(noffH->initData.virtualAddr);
	noffH->initData.inFileAddr = WordToHost(noffH->initData.inFileAddr);
	noffH->uninitData.size = WordToHost(noffH->uninitData.size);
	noffH->uninitData.virtualAddr = WordToHost(noffH->uninitData.virtualAddr);
	noffH->uninitData.inFileAddr = WordToHost(noffH->uninitData.inFileAddr);
}

//----------------------------------------------------------------------
// AddrSpace::AddrSpace
// 	Create an address space to run a user program.
//	Load the program from a file "executable", and set everything
//	up so that we can start executing user instructions.
//
//	Assumes that the object code file is in NOFF format.
//
//	First, set up the translation from program memory to physical 
//	memory.  For now, this is really simple (1:1), since we are
//	only uniprogramming, and we have a single unsegmented page table
//
//	"executable" is the file containing the object code to load into memory
//	"makeStack" - true znamena volani z EXECu, tzn. alokuje se pamet pro
//			kompletni program
//		    - false se vola pouze z konstruktoru, alokuje pamet pouze
//			pro kod a data
//----------------------------------------------------------------------

AddrSpace::AddrSpace(OpenFile *executable, bool makeStack )
{
    NoffHeader noffH;
    unsigned int i, size;

    if ( makeStack )
    {
        int numStackPages;
	int start;
	    // vytvorime misto pro data a kod
	space = new AddrSpace( executable, false );
	    // nasleduje alokace zasobniku a prekryti tabulek
	if ( space -> allocated == false )
	{
	    allocated = false;		// nepodarilo se alokovat dostatek
	    return;			// dostatek pameti pro kod a data
	}				// nema cenu pokracovat dal
	// pokusime se vytvorit stack
        numStackPages = divRoundUp( UserStackSize, PageSize );
    
	size = numStackPages*PageSize;
    
	DEBUG( 'a', "Initializing stack space, num pages %d, size %d",
	    numStackPages, size );
        start = memManager -> Allocate( numStackPages, (int)this );
    
        if ( start == -1 )
        {
	    allocated = false;
	    return;
	}
	allocated = true;
    
        numPages = space -> numPages;
	pageTable = new TranslationEntry[ numPages + numStackPages ];
	// sdilena pamet je stejna
	for ( i = 0 ; i < numPages ; i++ )
	{
	    pageTable[i].virtualPage  = i;
	    pageTable[i].physicalPage = space -> pageTable[i].physicalPage;
	    pageTable[i].valid	      = space -> pageTable[i].valid;
	    pageTable[i].use	      = space -> pageTable[i].use;
	    pageTable[i].dirty	      = space -> pageTable[i].dirty;
	    pageTable[i].readOnly     = space -> pageTable[i].readOnly;
	}
        // upravime polozky pro zasobnik
	for ( i = numPages ; i < numPages + numStackPages ; i++ )
	{
	    pageTable[i].virtualPage  = i;
	    pageTable[i].physicalPage = start + i - numPages;
	    pageTable[i].valid	      = true;
	    pageTable[i].use	      = false;
	    pageTable[i].dirty	      = false;
	    pageTable[i].readOnly     = false;
	}
    
        bzero( machine -> mainMemory + start*PageSize, size );
    
	numPages += numStackPages;
	shared = 0;
	return;
    }
    // zde probiha alokace pameti pro kod a data
	
    executable->ReadAt((char *)&noffH, sizeof(noffH), 0);
    if ((noffH.noffMagic != NOFFMAGIC) && 
		(WordToHost(noffH.noffMagic) == NOFFMAGIC))
    	SwapHeader(&noffH);
    ASSERT(noffH.noffMagic == NOFFMAGIC);

    size = noffH.code.size + noffH.initData.size + noffH.uninitData.size;
    numPages = divRoundUp(size, PageSize);
    size = numPages * PageSize;

    ASSERT(numPages <= NumPhysPages);		// check we're not trying
						// to run anything too big --
						// at least until we have
						// virtual memory

    DEBUG('a', "Initializing address space, num pages %d, size %d\n", 
					numPages, size);
    int start = memManager -> Allocate( numPages, (int)this );
    
    if ( start == -1 ) {	// nepovedla se alokace
	allocated = false;
	return;
    }
    allocated = true;
    
// first, set up the translation 
    pageTable = new TranslationEntry[numPages];
    for (i = 0; i < numPages; i++) {
	pageTable[i].virtualPage = i;	// for now, virtual page # = phys page #
	pageTable[i].physicalPage = start+i;
	pageTable[i].valid = TRUE;
	pageTable[i].use = FALSE;
	pageTable[i].dirty = FALSE;
	pageTable[i].readOnly = FALSE;  // if the code segment was entirely on 
					// a separate page, we could set its 
					// pages to be read-only
    }
    
// zero out the entire address space, to zero the unitialized data segment 
// and the stack segment
    bzero(machine->mainMemory+start*PageSize, size);

// then, copy in the code and data segments into memory
    if (noffH.code.size > 0) {
        DEBUG('a', "Initializing code segment, at 0x%x, size %d\n", 
			noffH.code.virtualAddr, noffH.code.size);
        executable->ReadAt(&(machine->mainMemory[noffH.code.virtualAddr + start*PageSize]),
			noffH.code.size, noffH.code.inFileAddr);
    }
    if (noffH.initData.size > 0) {
        DEBUG('a', "Initializing data segment, at 0x%x, size %d\n", 
			noffH.initData.virtualAddr, noffH.initData.size);
        executable->ReadAt(&(machine->mainMemory[noffH.initData.virtualAddr + start*PageSize]),
			noffH.initData.size, noffH.initData.inFileAddr);
    }
    shared = 0;		// shared se muze menit
    space = 0;		// space zustava pro kod a data nulove
}

//----------------------------------------------------------------------
// AddrSpace::AddrSpace( AddrSpace * toFork )
//	Alokuje pamet pouze pro zasobnik, kod a data bude sdilet
//----------------------------------------------------------------------

AddrSpace::AddrSpace( AddrSpace * toFork )
{
    int numStackPages;
    unsigned i, size;
    int start;
    
    numStackPages = divRoundUp( UserStackSize, PageSize );
    
    size = numStackPages*PageSize;
    
    DEBUG( 'a', "Initializing only stack space, num pages %d, size %d",
	    numStackPages, size );
    start = memManager -> Allocate( numStackPages, (int)this );
    
    if ( start == -1 )
    {
	allocated = false;
	shared = 0;
	return;
    }
    allocated = true;
    
    space = toFork -> space;
    numPages = space -> numPages;
    pageTable = new TranslationEntry[ numPages + numStackPages ];
    // sdilena pamet je stejna
    for ( i = 0 ; i < numPages ; i++ )
    {
	pageTable[i].virtualPage  = i;
	pageTable[i].physicalPage = space -> pageTable[i].physicalPage;
	pageTable[i].valid	  = space -> pageTable[i].valid;
	pageTable[i].use	  = space -> pageTable[i].use;
	pageTable[i].dirty	  = space -> pageTable[i].dirty;
	pageTable[i].readOnly	  = space -> pageTable[i].readOnly;
    }
    // upravime polozky pro zasobnik
    for ( i = numPages ; i < numPages + numStackPages ; i++ )
    {
	pageTable[i].virtualPage  = i;
	pageTable[i].physicalPage = start + i - numPages;
	pageTable[i].valid	  = true;
	pageTable[i].use	  = false;
	pageTable[i].dirty	  = false;
	pageTable[i].readOnly	  = false;
    }
    
    bzero( machine -> mainMemory + start*PageSize, size );
    
    numPages += numStackPages;
    shared = 0;
    space -> shared++;
}

//----------------------------------------------------------------------
// AddrSpace::~AddrSpace
// 	Dealloate an address space.  Nothing for now!
//----------------------------------------------------------------------

AddrSpace::~AddrSpace()
{
    if ( allocated )
    {
        delete pageTable;
        memManager->Deallocate((int) this);
	if ( space )
	    if ( space -> shared ) space -> shared--;
	    else delete space;
    }
}

//----------------------------------------------------------------------
// AddrSpace::InitRegisters
// 	Set the initial values for the user-level register set.
//
// 	We write these directly into the "machine" registers, so
//	that we can immediately jump to user code.  Note that these
//	will be saved/restored into the currentThread->userRegisters
//	when this thread is context switched out.
//----------------------------------------------------------------------

void
AddrSpace::InitRegisters( int funcAddr )
{

    int i;

    for (i = 0; i < NumTotalRegs; i++)
	machine->WriteRegister(i, 0);

    // Initial program counter -- must be location of "Start"
    machine->WriteRegister(PCReg, funcAddr );

    // Need to also tell MIPS where next instruction is, because
    // of branch delay possibility
    machine->WriteRegister(NextPCReg, funcAddr + 4);

   // Set the stack register to the end of the address space, where we
   // allocated the stack; but subtract off a bit, to make sure we don't
   // accidentally reference off the end!
    machine->WriteRegister(StackReg, numPages * PageSize - 16);
    DEBUG('a', "Initializing stack register to %d\n", numPages * PageSize - 16);
}

//----------------------------------------------------------------------
// AddrSpace::SaveState
// 	On a context switch, save any machine state, specific
//	to this address space, that needs saving.
//
//	For now, nothing!
//----------------------------------------------------------------------

void AddrSpace::SaveState() 
{}

//----------------------------------------------------------------------
// AddrSpace::RestoreState
// 	On a context switch, restore the machine state so that
//	this address space can run.
//
//      For now, tell the machine where to find the page table.
//----------------------------------------------------------------------

void AddrSpace::RestoreState() 
{
    machine->pageTable = pageTable;
    machine->pageTableSize = numPages;
}
