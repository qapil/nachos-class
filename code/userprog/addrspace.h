// addrspace.h 
//	Data structures to keep track of executing user programs 
//	(address spaces).
//
//	For now, we don't keep any information about address spaces.
//	The user level CPU state is saved and restored in the thread
//	executing the user program (see thread.h).
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#ifndef ADDRSPACE_H
#define ADDRSPACE_H

#include "copyright.h"
#include "filesys.h"


// pridano by JK & MK
#include "bitmap.h"

class MemManager {

    struct MemoryEntry {	// struktura udrzujici informace o obsazenych strankach
	int startPage;		// kterou strankou blok zacina
	int count;		// kolik stranek zabira
	int id;			// ktery AddrSpace blok pouziva
	MemoryEntry * next;
	MemoryEntry()
	{
	    startPage = count = id = 0;
	    next = 0;
	};
    };
public:
    MemManager()
    {
	bitMap = new BitMap( NumPhysPages );
	usedMem = 0;		// na zacatku je vsechna pamet volna
    };
    
    int Allocate( int count, int ident );	// zjisti, jestli ma dostatek
		// souvisleho mista v pameti, kdyz ne, vrati -1, kdyz pameti
		// je dost, vrati prvni stranku volne pameti, zaroven pamet
		// nastavi jako oznacenou
    
    void Deallocate( int ident );
		// dealokuje pamet pouzivajici trida (AddrSpace*)ident
    
    ~MemManager()
    {
	delete bitMap;
	if ( usedMem) delete usedMem;
    };
    
private:
    BitMap * bitMap;
    MemoryEntry * usedMem;
};

// konec JK & MK

#define UserStackSize		1024 	// increase this as necessary!

class AddrSpace {
  public:
    AddrSpace(OpenFile *executable, bool makeStack = true);
					// Create an address space,
					// initializing it with the program
					// stored in the file "executable"
    AddrSpace( AddrSpace * toFork );	// alokuje se jenom pamet pro stack
					// ostatni bude sdilene s volajicim
					// threadem
    ~AddrSpace();			// De-allocate an address space
					// s ohledem na sdileni

    void InitRegisters(int funcAddr = 0 );// Initialize user-level CPU registers,
					// before jumping to user code
					// parametr je nenulovy v pripade volani
					// ze syscallu FORK

    void SaveState();			// Save/restore address space-specific
    void RestoreState();		// info on a context switch

// JK & MK
    bool allocated;			// byla-li alokace pameti uspesna
    int shared;				// polozky pouzite v pripade,
    AddrSpace * space;			// ze se zavola na thread FORK
// konec JK & MK
  private:
    TranslationEntry *pageTable;	// Assume linear page table translation
					// for now!
    unsigned int numPages;		// Number of pages in the virtual 
					// address space
};

#endif // ADDRSPACE_H
