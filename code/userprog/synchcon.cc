// vytvoreno J.K. & M.K.

#include "synchcon.h"
//
// rutiny pro synchronizaci pristupu na konzoli
//

// tyto dve funkce jsou volany asynchronni konsoli po dokonceni operace
static void readAvail( int arg )
{
    SynchCon * con = ( SynchCon *)arg;
    
    con -> CheckCharAvail();
}

static void writeDone( int arg )
{
    SynchCon * con = ( SynchCon *)arg;
    
    con -> WriteDone();
}

SynchCon::SynchCon( char *readFile, char *writeFile )
{
    console = new Console( readFile, writeFile, 
			&readAvail, &writeDone, (int)this );

    readsem = new Semaphore( "synch console read semaphore", 0 );
    writesem = new Semaphore( "synch console write semaphore", 0 );
    
    readlock = new Lock( "synch console read lock" );
    writelock = new Lock( "synch console write lock" );
}

SynchCon::~SynchCon()
{
    delete console;
    delete readsem; delete writesem;
    delete readlock; delete writelock;
}

void SynchCon::PutChar( char ch )
{
    writelock -> Acquire();
    console -> PutChar( ch );
    writesem -> P();
    writelock -> Release();
}

char SynchCon::GetChar()
{
    readlock -> Acquire();
    readsem -> P();
    char ch = console -> GetChar();
    readlock -> Release();
    return ch;
}

void SynchCon::WriteDone()
{
    writesem -> V();			// uvolni proces z PutChar()
}

void SynchCon::CheckCharAvail()
{
    readsem -> V();			// uvolni proces z GetChar()
}
