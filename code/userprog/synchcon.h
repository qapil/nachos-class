// vytvoreno J.K. & M.K.

#ifndef SYNCHCON_H
#define SYNCHCON_H

#include "copyright.h"
#include "console.h"
#include "synch.h"

// Nasledujici trida zabezpecuje synchronizaci pro pristup ke konzoli.
// Ceka, dokud neni operace provedena do konce

class SynchCon {
public:
    SynchCon(char *readFile, char *writeFile);	// inicializuje vsechna potrebna data
    ~SynchCon();

    void PutChar(char ch);		// zobrazi znak na konzolu
    char GetChar();			// precte znak z konzole
    
    // !!! nevolat samostatne !!!
    void WriteDone();			// pro interni pouziti
    void CheckCharAvail();

private:
    Console   * console;		// asynchronni konzole
    Semaphore * readsem,		// prostredek synchronizace
	      * writesem;
    Lock      * readlock,		// pouze jeden thread muze pristupovat
	      * writelock;		// ke konzoli
};

#endif
