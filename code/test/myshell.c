#include "syscall.h"

char info[] = "Nachos shell\n   ";
char prompt[] = "--$ ";
char fail[] = "exec failed\n";
char newline[] = "\n  ";
char exitCode[] = "\nexit code: ";
char input_file[] = "sh.in";

int main()
{
    SpaceId newProc;
    char  ch[4], buf1[60], buf2[60];
    int i,  j, k , bg, a;
    OpenFileId input = Open(input_file);
    OpenFileId output = ConsoleOutput;

    Write(info, 13, output);
    while( 1 )
    {
	Write(prompt, 4, output);

	i = j = 0;
	do {
	    Read(&buf1[i], 1, input); 
	} while( buf1[i++] != '\n' );

	buf1[--i] = '\0';

        while (j < i) {
    	    k = 0;
	    for ( ; buf1[j] == ' '; j ++) ;
	    while (j < i) {
	        buf2[k++] = buf1[j++];
		
		if (buf2[k-1] == ';') {
		    buf2[--k] = '\0';
		    break;
		}
	    }
	    
	    if (buf2[--k] == '&') {
	        buf2[k++] = '\0';
	        bg = 1;
	    }
	    else bg = 0;
	    
	    Write(buf2, k+1, output);
	    Write(newline, 1, output);

	    if ((newProc = Exec(buf2)) == -1)
	        Write(fail, 12, output);
	    else
		if (! bg) Join(newProc);

	    for( a = 0 ; a < 60 ; a++ )
		buf2[a] = 0;
	}
	if (i == 0) break;
    }
    Exit(0);
}
