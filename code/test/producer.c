#include "syscall.h"

#define BUF_SIZE	8

char input_file[] = "producer_input.txt";
char buffer[BUF_SIZE];
int p_index, c_index;
int empty, full;
int length;
OpenFileId input, output ;

char ch_empty = '.';
char ch_full = 'x';
char newline = '\n';
char hlaska[] = "   ";

void content()
{
    int i, flip;
    
    if (p_index < c_index) flip = 0;
    else flip = 1;

    for (i = 0; i < BUF_SIZE; i ++) {
	    if (i == p_index || i == c_index) flip = ++flip % 2;
	    if (flip) Write(&ch_full, 1, output);
	    else Write(&ch_empty, 1, output);
    }
    Write(&newline, 1, output);
}

void produce_item()
{
    char ch;
    
    Read(&ch, 1, input);
    buffer[p_index++] = ch;
    p_index %= BUF_SIZE;

    hlaska[0] = '+';
    hlaska[1] = ch;
    Write(hlaska, 3, output);
    content();
}
void consume_item()
{
    char ch;
    
    ch = buffer[c_index++];
    c_index %= BUF_SIZE;
    
    hlaska[0] = '-';
    hlaska[1] = ch;
    Write(hlaska, 3, output);
    content();
}

void consumer()
{
    int i, j, x;
    
    if (! SemGet(full, SemCreate + 0) )
	SemGet(full, SemAttach);
    if (! SemGet(empty, SemCreate + BUF_SIZE))
	SemGet(empty, SemAttach);
	
    for (i = 0; i < length; i ++) {
	if (i == 6)
	    for (j = 0; j < 1000; j ++) {
		x = x*87;
		x = x%13;
	    }
	SemCtl(full, SemP);
	consume_item();
	SemCtl(empty, SemV);
    }
    
    Exit(0);
}

void producer()
{
    int i, j, x;
    
    if (! SemGet(full, SemCreate + 0))
	SemGet(full, SemAttach);
    if (! SemGet(empty, SemCreate + BUF_SIZE))
	SemGet(empty, SemAttach);
	
    for (i = 0; i < length; i ++) {
	if (i == 5)
	    for (j = 0; j < 1000; j ++) {
		x = x*87;
		x = x%13;
	    }
	SemCtl(empty, SemP);
	produce_item();
	SemCtl(full, SemV);
    }
    
    Exit(0);
}

int main()
{
    input = Open(input_file);
    output = ConsoleOutput;
    length = Length(input);
    empty = 1; full = 2;
    p_index = c_index = 0;

    Fork(consumer);    
    producer();
    
    Close(input);
    Exit(0);
}

