#include "syscall.h"

#define BUF_SIZE	8

char ok[] = "Ok.\n";

int buffer[BUF_SIZE];
int p_index = 0, c_index = 0;
int empty = 0, full = 0;
int length = 0;
OpenFileId input = 0, output = 0;

char prod[] = "Producent vyprodukoval\n";	// 23
char cons[] = "Consument zkonzumoval\n";	// 22
char state[BUF_SIZE+1];

int manip = 0;

void consumer()
{
    int i, j, x;
    char a;
    
    if (! SemGet(full, SemCreate + 0) )
	SemGet(full, SemAttach);
    if (! SemGet(empty, SemCreate + BUF_SIZE))
	SemGet(empty, SemAttach);
	
    SemGet( manip, SemAttach );
	
    for (i = 0; i < length; i ++) {
	if (i == 6)
	    for (j = 0; j < 1000; j ++) {
		x = x*87;
		x = x%13;
	    }
	SemCtl(full, SemP);

        buffer[c_index++]; 
        c_index %= BUF_SIZE;

	SemCtl( manip, SemP );  
        Write(cons, 22, output);
 /*	x = !(c_index < p_index);		// tento usek kodu vypisuje
	for ( i = 0 ; i < BUF_SIZE ; i++ )	// jak vypada buffer
	{					// je zajimave, ze kdyz se to
	    if ( i == c_index || i == p_index ) x = !x;
	    if ( x ) a = 'X';			// odkomentuje, tak se program
	    else  a = '.';			// zacykli, a produkuje se  a
	    state[i] = a;			// konzumuje donekonecna
	}
	state[BUF_SIZE] = '\n'; Write( state, BUF_SIZE+1, output );
*/	SemCtl( manip, SemV );

	SemCtl(empty, SemV);
    }
    
    Exit(0);
}

void producer()
{
    int i, j, x;
    char a;

    if (! SemGet(full, SemCreate + 0))
	SemGet(full, SemAttach);
    if (! SemGet(empty, SemCreate + BUF_SIZE))
	SemGet(empty, SemAttach);
	
    for (i = 0; i < length; i ++) {
	if (i == 5)
	    for (j = 0; j < 1000; j ++) {
		x = x*87;
		x = x%13;
	    }
	SemCtl(empty, SemP);
    
        buffer[p_index++] = i;
        p_index %= BUF_SIZE;

	SemCtl( manip, SemP );
	Write( prod, 23, output );
/*	x = !( c_index < p_index );
	for ( i = 0 ; i < BUF_SIZE ; i++ )
	{
	    if ( i == c_index || i == p_index ) x = !x;
	    if ( x ) a = 'X';
	    else  a = '.';
	    state[i] = a;
	}
	state[BUF_SIZE] = '\n'; Write( state, BUF_SIZE+1, output );
*/	SemCtl( manip, SemV );

	SemCtl(full, SemV);
    }
    
    Exit(0);
}

int main()
{
    int b;
    input = ConsoleInput;
    output = ConsoleOutput;
    length = Length(input);
    length = 20;
    empty = 1; full = 2;
    p_index = c_index = 0;
    
    manip =  0x1111;

    Write( ok, 4, output );
    
    for ( b = 0 ; b < BUF_SIZE +1 ; b++ )
	state[b] = '.';

    SemGet( manip, SemCreate + 1 );

    Fork(consumer);    
    producer();
    
    Exit(0);
}

