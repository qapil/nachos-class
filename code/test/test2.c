#include "syscall.h"

int main()
{
    OpenFileId fd1, fd2;
    char f1[4], f2[4], buf[6];
    int c;

    f1[0] = 't';
    f1[1] = '1';
    f1[2] = 0;
    
    f2[0] = 't';
    f2[1] = '2';
    f2[2] = 0;
    
    fd1 = Open(f1);
    if ((c=Read(buf, 5, fd1)) != 5) {
	f2[1] = '3';
    }
    Close(fd1);

    Write(buf, c, ConsoleOutput);
    
    Create(f2);
    fd2 = Open(f2);
    Write(buf, c, fd2);
    Close(fd2);

    Exit(0);
}
