#include "syscall.h"

char config[] = "nsh.msg";
OpenFileId input = ConsoleInput;
OpenFileId output = ConsoleOutput;

int main()
{
    SpaceId newProc;
    char  ch[4], buf1[60], buf2[60];
    int i, j, k, l, bg, fd;
    char info[13];
    char prompt[4];
    char newline[1];
    char fail[12];
    
    fd = Open(config);
    Read(info, 13, fd);
    Read(prompt, 4, fd);
    Read(newline, 1, fd);
    Read(fail, 12, fd);
    Close(fd);

    Write(info, 13, output);
    while( 1 )
    {
	Write(prompt, 4, output);

	i = j = 0;
	do {
	    Read(&buf1[i], 1, input); 
	} while( buf1[i++] != '\n' );

	buf1[--i] = '\0';

        while (j < i) {
    	    k = 0;
	    for ( ; buf1[j] == ' '; j ++) ;
	    while (j < i) {
	        buf2[k++] = buf1[j++];
		
		if (buf2[k-1] == ';') {
		    buf2[--k] = '\0';
		    break;
		}
	    }
	    
	    if (buf2[--k] == '&') {
	        buf2[k++] = '\0';
	        bg = 1;
	    }
	    else bg = 0;
	    
	    Write(buf2, k+1, output);
	    Write(newline, 1, output);
	    
	    if ((newProc = Exec(buf2)) == -1)
	        Write(fail, 12, output);
	    else
		if (! bg) l = Join(newProc);

	    for(l = 0 ; l < 60 ; l ++)
		buf2[l] = 0;
	}
	if (i == 0) break;
    }
    Exit(0);
}
