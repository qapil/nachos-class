#include "syscall.h"

void fce()
{
    int i, j, k;
    char ch[1];
    
    ch[0] = 'f';
    for (i=0; i < 10; i ++) {
	for (j=0; j < 500; j ++) k = (k+10)%99;
	Write(ch, 1, ConsoleOutput);
    }
    Exit(0);
}

int main()
{
    int id1, id2, id3;
    char p1[6], p2[6], p3[6], txt[4];
    
    p1[0] = 't'; p1[1] = 'e'; p1[2] = 's'; p1[3] = 't'; p1[4] = '4'; p1[5] = 0;
    p2[0] = 't'; p2[1] = 'e'; p2[2] = 's'; p2[3] = 't'; p2[4] = '5'; p2[5] = 0;
    p3[0] = 't'; p3[1] = 'e'; p3[2] = 's'; p3[3] = 't'; p3[4] = '6'; p3[5] = 0;

    Fork(fce);
    
    id1 = Exec(p1);
    id2 = Exec(p2);

    if (id1 >= 0) txt[0] = '0' + Join(id1);
    if (id2 >= 0) txt[1] = '0' + Join(id2);
    id3 = Exec(p3);
    if (id3 >= 0) txt[2] = '0' + Join(id3);
    txt[3] = '\n';
    
    Write(txt, 4, ConsoleOutput);
    
    Exit(0);
}
