#include "syscall.h"

char buf1[11], buf2[11];
int key = 100;

void function(void)
{
    int i;
    for ( i = 1 ; i < 11 ; i++, i++ )
	buf1[i] = 'a';
    
    if (! SemGet(key, SemCreate + 1) )
	SemGet(key, SemAttach);
	
    for ( i = 0; i < 11; i ++) {
	SemCtl(key, SemP);
	Write( buf2 + i, 1, ConsoleOutput );
	SemCtl(key, SemV);
    }
    
    SemCtl(key, SemDetach);

    Exit(1);
}

void main()
{
    int i;
    
    for ( i = 0 ; i < 10 ; i++ )
    {
        buf1[i] = 'X';
	buf2[i] = 'Y';
    }
    buf1[10] = buf2[10] = '\n';
    
    Fork( function );

// --------------------------------------
    
    for ( i = 1 ; i < 11 ; i++, i++ )
	buf2[i] = 'b';
    
    if (! SemGet(key, SemCreate + 1) )
	SemGet(key, SemAttach);

    for ( i = 0; i < 11; i ++) {
	SemCtl(key, SemP);
	Write( buf1 + i, 1, ConsoleOutput );
	SemCtl(key, SemV);
    }
    
    SemCtl(key, SemDetach);
    
    Exit( 0 );
}

