#include "syscall.h"

int main()
{
    OpenFileId fd;
    char buf[6];
    char f1[3];

    buf[0] = 's';
    buf[1] = 'l';
    buf[2] = 'o';
    buf[3] = 'v';
    buf[4] = 'o';
    buf[5] = 0;
    
    f1[0] = 't';
    f1[1] = '1';
    f1[2] = 0;
    
    Write(buf, 5, ConsoleOutput);

    Create(f1);
    fd = Open(f1);
    Write(buf, 5, fd);
    Close(fd);

    Write(buf, 5, ConsoleOutput);
    
    Exit(0);
}