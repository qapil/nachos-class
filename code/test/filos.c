#include "syscall.h"

#define	FILOSOFU	5
#define MANIP		0x1111
#define CHODU		5

char msg1[] = "Filosof . vzal pravou vidlicku\n";	// 31
char msg2[] = "Filosof . vzal levou vidlicku\n";	// 30
char msg3[] = "Filosof . zacina premyslet\n";		// 27
char msg4[] = "Filosof . prestal premyslet\n";		// 28
char msg5[] = "Filosof . zacina obedvat\n";		// 25
char msg6[] = "Filosof . doobedval\n";			// 20
char msg7[] = "Filosof . polozil pravou vidlicku\n";	// 34
char msg8[] = "Filosof . polozil levou vidlicku\n";	// 33

char msg20[] = "Filosof . ready\n";			// 16

int vidlicky[FILOSOFU];
int id = 0;				// identifikace filosofa
char chid = 0;
int manip = 0;				// manipulacni semafor;

#define PRINT(msg, l)			\
    SemCtl( manip, SemP );		\
    msg[8] = myChId; Write( msg, l, ConsoleOutput);	\
    SemCtl( manip, SemV );

#define TAKEFORK( id )	SemCtl( vidlicky[id], SemP );
    
#define PUTFORK( id )	SemCtl( vidlicky[id], SemV );
    
#define EAT				\
    {					\
	int a, b, c;			\
	for( a = 1 ; a < 20*myId ; a++ )	\
	{ b = 34*5;  c = b%a; }		\
    }
    
#define THINK				\
    {					\
	int a, b, c;			\
	for ( a = 1 ; a < 20*myId ; a++ )	\
	{ b = 34/5; c = b%a; }		\
    }

void filosof( void )
{
    int left, right;
    char myChId;			// jedno id pro zapis do retazcu
    int myId;				// jine pro vypocty
    int i;

    SemGet( manip, SemAttach );

    SemCtl( manip, SemP );
    id++; chid++;			// zjistim si svoje poradi
    myId = id; myChId = chid;
    msg20[8] = myChId;
    Write( msg20, 16, ConsoleOutput );
    SemCtl( manip, SemV );

    if ( id < FILOSOFU )    
        for( i = 0 ; i < FILOSOFU ; i++ )
	    SemGet( vidlicky[i], SemAttach );

    left = id % FILOSOFU;
    right = ( id + 1 ) % FILOSOFU;
    
    for ( i = 0 ; i < CHODU ; i++ )
    {
	TAKEFORK( right )
	PRINT( msg1, 31 )

	TAKEFORK( left )
	PRINT( msg2, 30 )
	
	PRINT( msg5, 25 )
	EAT
	PRINT( msg6, 20 )
	
	PUTFORK( right )
	PRINT( msg7, 34 )
	
	PUTFORK( left )
	PRINT( msg8, 33 )
	
	PRINT( msg3, 27 )
	THINK
	PRINT( msg4, 28 )
    }

    Exit(0);
}

void main( void )
{
    int i;
    
    manip = MANIP;
    for ( i = 0 ; i < FILOSOFU ; i++ )
    {
	vidlicky[i] = i;
	SemGet( vidlicky[i], SemCreate + 1 );
    }
    
    chid = '0';

    SemGet( manip, SemCreate + 1 );

    for ( i = 0 ; i < FILOSOFU - 1 ; i++ )
        Fork( filosof );
    SemCtl( manip, SemDetach );
    filosof();
    
    Exit(0);
}    