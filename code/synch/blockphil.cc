#include "copyright.h"
#include "system.h"
#include "synch.h"
#include <stdlib.h>

#define N	2

// trida Fork ma na starosti vidlicky
// zde implementovana metoda hrozi zablokovanim vsech filosofu
// pro N = 2 dojde k zablokovani okamzite
class Fork {
public:
  Fork();        			// inicializuje promenne
  ~Fork();		        	// uvolni alokovane zamky
  void takeFork(int which);	// bere vidlicku se zablokovanim
  void putFork(int which);	// polozi vidlicku zpet na stul
private:
  Lock * forkLock[N];		// kazda vidlicka ma svuj vlastni zamek
};

// trida Philosopher simuluje zivot filosofa
class Philosopher {
public:
  // vytvori filosofa pouzivajici vidlicky f, bude mit m chodu
  // jeho id je which
  Philosopher(Fork * f, int which, int m);
  void think();				// filosof nejakou dobu premysli
  void eat();				// filosof nejakou dobu ji
  void run();				// filosofuv kolobeh zivota
private:
  int id, right, left, meals;
  Fork * forks;
};

Fork::Fork()
{
  for (int i=0; i < N; i ++)
    forkLock[i] = new Lock("vidlicka");
}

Fork::~Fork()
{
  for ( int i = 0 ; i < N ; i++ )
    delete forkLock[i];
}

void Fork::takeFork(int which)
{
    forkLock[which] -> Acquire();		// vidlicka slouzi k jidlu
}
void Fork::putFork(int which)
{
    forkLock[which] -> Release();		// vidlicka je pripravena
}

// *******************

Lock *lock = new Lock("philosopher");
Condition *cv = new Condition("philosopher");

// generace nahodnych cisel od 0 do num-1
int random( int num )
{
   return (int) ((double)num*rand()/(RAND_MAX + 1.0));
}

Philosopher::Philosopher(Fork * f, int which, int m)
{
    forks = f;
    id = which;
    meals = m;
    left = id;			// filosofova leva vidlicka
    right = (id+1)%N;		// filosofova prava vidlicka
}

void Philosopher::think()
{
    int r = random( 10 );
    for (int i = 0 ; i < r ; i++ )
	  currentThread -> Yield();		// filosof premysli
}

void Philosopher::eat()
{
    int r = random( 20 );
    for ( int j = 0 ; j < r ; j++)
	  currentThread->Yield();		// filosof ji
}

void Philosopher::run()
{
  int prvni, druha;
  
  for (int i = 1 ; i < meals+1 ; i++)
   {
    printf( "%i - * zacinam myslet\n", id );
    think();		// nechame filosofa premyslet
    printf( "%i - * dopremyslel jsem\n", id );
    currentThread -> Yield();

    // filosof vybere vidlicky v nahodnem poradi
    if ( random(2) ) { prvni = left; druha = right; }
    else { prvni = right; druha = left; }
  
    // zvedneme vidlicky
    printf( "%i - O beru levou vidlicku\n", id );
    forks -> takeFork( left );
    currentThread -> Yield();
    printf( "%i - X beru pravou vidlicku\n", id );
    forks -> takeFork( right );
    currentThread -> Yield();
	
    printf( "%i - + zacinam jist %i. jidlo\n", id, i );	
    eat();			// filosof dostal hlad, tak at se naji
    printf( "%i - + dojedl jsem %i. jidlo\n", id, i );
	
    forks -> putFork( left );
    printf( "%i - O polozil jsem levou vidlicku\n", id );
    currentThread -> Yield();
    printf( "%i - X polozil jsem pravou vidlicku\n", id );
    forks -> putFork( right );
    currentThread -> Yield();
  }
}

// ************************

void live( int p )
{
    ((Philosopher *)p) -> run();
}

void ThreadTest()
{
  Thread * t;
  Philosopher * p;
  Fork * f = new Fork();

  srand( 1111 );
        
  for ( int i = 0 ; i < N ; i++ )
  {
    p = new Philosopher( f, i, 10 );
    t = new Thread( "philosopher" );
    t -> Fork( live, (int)p );
  }

}