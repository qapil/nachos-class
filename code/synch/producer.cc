#include "copyright.h"
#include "system.h"
#include "synch.h"
#include <malloc.h>
#include <stdlib.h>

class Buffer {
  public:
    Buffer (int size);  // Vytvori buffer dane delky
    ~Buffer();
    void add (char c);  // prida znak na konec bufferu
    char remove ();     // odstrani znak ze zacatku bufferu

  private:
    int *buffer;        // obsah bufferu
    int bufsize;        // velikost
    int in;             // pozice pro vkladani
    int out;            // pozice pro vybirani
    int count;
    Condition *cv;
    Lock *bufLock;
};

Buffer *b;              // sdileny buffer
Lock *readLock;

class Producer {
  public:
    // vytvori producenta, ktery bude cist ze souboru f, zapisovat do 
    // bufferu b a bude mit identifikaci which
    Producer (Buffer *b, FILE *f, int which);   

    void run ();

  private:
    Buffer *theBuffer;
    FILE *theFile;
    int id;
};

// The class defining a consumer
class Consumer {
  public:
    // vytvori konzumenta, ktery bude cist z bufferu b a zapisovat na 
    // standardni vystup a jedo identifikace bude which
    Consumer (Buffer *b, int which);  

    void run ();

  private:
    Buffer *theBuffer;
    int id;
};

/* nasledujici funkce zajisti, ze se producenti a konzumenti rozbehnou, 
jsou pouzity jako parametru do funkce fork */

void produce(int p) {
  ((Producer *)p)->run();
}

void consume(int c) {
  ((Consumer *)c)->run ();
}


void ThreadTest()
{
    DEBUG('t', "Zacina producent/konzument\n");

    printf ("Velikost bufferu:  ");
    int bufsize;
    scanf ("%d", &bufsize);
    if (bufsize <= 0) {
      printf ("velikost bufferu musi byt nezaporne cislo.\n");
      exit (-1);
    }
    b = new Buffer(bufsize);

    srand(10000);
    
    printf ("pocet producentu:  ");
    int numProducers;
    scanf ("%d", &numProducers);
    if (numProducers <= 0) {
      printf ("procet musi byt nezaporne cislo.\n");
      exit (-1);
    }
    FILE *inFile = fopen ("producer_input.txt", "r");
    Thread *t;
    Producer *p;
    
    for (int i = 0; i < numProducers; i++) {
	// Start the producers
	p = new Producer(b, inFile, i);
	t = new Thread("producent");
	t->Fork(produce,(int) p);
    }

    printf ("pocet konzumentu:  ");
    int numConsumers;
    scanf ("%d", &numConsumers);
    if (numConsumers <= 0) {
      printf ("pocet musi byt nezaporne cislo.\n");
      exit (-1);
    }
    Consumer *c;
    for (int j = 0; j < numConsumers; j++) {
	// Start the consumers
        c = new Consumer(b, j);
        t = new Thread("consumer");
        t->Fork(consume, (int) c);
    }
}

Buffer::Buffer (int size)
{
    in = 0;
    out = 0;

    // alokace bufferu
    buffer = (int *) calloc (size, sizeof(int));
    bufsize = size;
    
    count = 0;
    cv = new Condition("BufferCondition");
    bufLock = new Lock("BufferLock");
}
Buffer::~Buffer()
{
    delete bufLock;
    delete cv;
    free(buffer);
}

void Buffer::add (char c)
{
    bufLock->Acquire();
    while (count == bufsize) {
	printf("*** buffer je plny\n"); 
	cv->Wait(bufLock);
    }
    buffer[in] = c;
    count ++;
    in ++; in %= bufsize;
    if (count == 1) cv->Broadcast(bufLock);
    bufLock->Release();
}

char Buffer::remove ()
{
    char c;
    
    bufLock->Acquire();
    while (count == 0) {
	printf("*** buffer je prazdny\n");
	cv->Wait(bufLock);
    }
    c = buffer[out];
    count --;
    out ++; out %= bufsize;
    if (count == bufsize-1) cv->Broadcast(bufLock);
    bufLock->Release();
    return c;
}

Producer::Producer (Buffer *buf, FILE *f, int which) {
  theBuffer = buf;
  theFile = f;
  id = which;
}

void Producer::run () {
    char c;

    // Read a character at a time until reaching the end of the file.
    for (c = getc (theFile); c != EOF; c = getc (theFile)) {
	// add the character to the buffer
	theBuffer->add(c);
	printf ("producent %d produkuje %c\n", id, c);
	if (rand() < RAND_MAX/2) currentThread->Yield();
    }
}

Consumer::Consumer (Buffer *buf, int which) {
  theBuffer = buf;
  id = which;
}

void Consumer::run () {
    char c;
    while (true) {
	// Remove the character from the buffer
	c = theBuffer->remove();
	printf ("konzument %d konzumuje %c\n", id, c);
	if (rand() < RAND_MAX/2) currentThread->Yield();
    }
}
