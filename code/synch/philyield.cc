#include "copyright.h"
#include "system.h"
#include "synch.h"
#include <stdlib.h>

#define N	5

class Fork {
public:
    Fork();
    ~Fork();
    bool takeFork(int which);
    void putFork(int which);
private:
    bool used[N];
    Lock *forkLock;
};

class Philosopher {
public:
    Philosopher(Fork *_forks, int which, int _meals);
    void think();
    void takeForks(int i);
    void eat(int i);
    void putForks(int i);
    void run();
private:
    int id, right, left, meals;
    Fork *forks;
};

Fork::Fork()
{
    for (int i=0; i < N; i ++)
	used[i] = false;
    forkLock = new Lock("vidlicky");
}
Fork::~Fork()
{
    delete forkLock;
}

bool Fork::takeFork(int which)
{
    bool ret;
    
//    forkLock->Acquire();
    if (used[which]) ret = false;
    else {
	used[which] = true;
	ret = true;
    }
//    forkLock->Release();
    return ret;
}
void Fork::putFork(int which)
{
    used[which] = false;
}

// *******************

Lock *lock = new Lock("philosopher");
Condition *cv = new Condition("philosopher");

Philosopher::Philosopher(Fork *_forks, int which, int _meals)
{
    forks = _forks;
    id = which;
    meals = _meals;
    left = id;
    right = (id+1)%N;
}
void Philosopher::think()
{
    int r = rand()/(RAND_MAX/10);
    for (int i=0; i < r; i ++)
	currentThread->Yield();
}
void Philosopher::takeForks(int i)
{
    lock->Acquire();
    while (1) {
	while (! forks->takeFork(left)) {
    	    printf("%i - nemam vidlicku pro %i. jidlo\n", id, i+1);

	    cv->Wait(lock);
	}
        currentThread -> Yield();
	if (! forks->takeFork(right)) {
	    printf("%i - nemam vidlicku pro %i. jidlo\n", id, i+1);

	    forks->putFork(left);
	    cv->Wait(lock);
	    continue;
	}
	break;
    }
    lock->Release();
}
void Philosopher::eat(int i)
{
    printf("%i - zacinam jist %i. jidlo\n", id, i+1);	
    
    int r = rand()/(RAND_MAX/20);
    for (int j=0; j < r; j ++)
	currentThread->Yield();

    printf("%i - dojedl jsem %i. jidlo\n", id, i+1);
}
void Philosopher::putForks(int i)
{
    lock->Acquire();
    forks->putFork(left);
    currentThread -> Yield();
    forks->putFork(right);
    currentThread -> Yield();
    cv->Broadcast(lock);
    lock->Release();
}
void Philosopher::run()
{
    for (int i=0; i < meals; i ++) {
	think();
        currentThread -> Yield();
	takeForks(i);
        currentThread -> Yield();
	eat(i);
        currentThread -> Yield();
	putForks(i);
        currentThread -> Yield();
    }
}

// ************************

void live(int p)
{
    ((Philosopher *)p)->run();
}

void ThreadTest()
{
    Thread *t;
    Philosopher *p;
    Fork *f = new Fork();

    srand(1000);
        
    for (int i=0; i < N; i ++) {
	p = new Philosopher(f, i, 10);
	t = new Thread("philosopher");
	t->Fork(live, (int) p);
    }
}