Planovani
---------

N nachosu jsme implementovali strategii planovani zalozenou na 
prioritach threadu. Thread s vyssi prioritou se dostane casteji k 
procesoru nez thread s prioritou nizsi.

    Informace o priorite threadu jsou udrzovany ve strukture ThreadInfo,
ktera je popsana v souboru uzivatelske_procesy.txt. V teto strukture se 
udrzuji i informace o tom, kdy naposledy byl thread na porcesoru, aby se 
mu mohla pripadne zvysovat priorita a urychlit tak pristup k procesoru. 
Hlavnim a jedinnym kriteriem pro vyber threadu je jeho priorita. Ta se 
deli na cast pevnou a dynamickou. Pevna cast je cast, kterou dostane 
thread pridelenou pri svem vzniku, pripadne si i muze upravit, pokud na 
ti\o ma prislusna prava, coz v nachosu ma vzdy. Dynamicka cast se meni 
v zavislosti na tom jak dlouho thrread nebyl na procesoru.

    Pri startu nachosu se inicializuje casovac, ktery emuluje preruseni 
od casovace na realnem systemu. Jako obsluzna rutina pro preruseni 
casovace je pouzita metoda TimerInterruptHandle(), ktera zajisti, ze 
aktualni thread bude ochoten vzdat se procesoru. Vyvola se metoda 
Yield() ve tride Thread, ktera vybere za pomoci scheduleru threadu, ktery 
pobezi na procesoru. Scheduler k tomuto vyberu pouzije metodu 
FindNextToRun() a ta ziska thread vybranim nejvhodnejsiho kandidata z 
ready fronty.

    Ready fronta je instance tridy PrioList, coz je potomek tridy List 
od ktere se lisi pouze v metode vybirani prvku. Nevibira se prvni prvek 
ve fronte, ale  prochazi se cela fronta, vsem threadum se prekontroluje 
jak dlouho jiz nebyli u procesoru a v pripade, ze tato doba prekroci 
konstantu CHANGE_PRIORITY_TICKS, zvysi se threadu dynamicka priorita. 
Ze vsech threadu se vybere ten thread, ktery ma nejvyssi soucet 
dynamicke a staticke priority.

    Do metody Yield se tedy dostane thread s nejvyssi prioritou ze 
vsech pripravenych threadu. Je nutne jeste poroovnat priority tohoto 
a momentalne preruseneho threadu a spustit thread s vyssi prioritou a 
ten druhy pridat do fronty pripravenych threadu. Threadu, ktery 
odstranujeme z procesoru musime nastavit promennou ticks, ktera udava, 
kdy naposled thread bezel nebo kdy mu byla zvysena dynamicka priorita. 
Threadu, ktery se na procesor doslal snizime dynamickou prioritu na 
nulu. Tudiz na procesoru bezi thready vzdy jen na se svoji pevnou 
prioritou, dynamicka cast pouze vyjadruje jak dlouho jiz thread u 
procesoru nebyl.

    V nasi implementaci jsme vyuzili podmineneho prakladu a je mozne 
planovani vypnout nebo zapnout v zavislosti na tom zde je definovana 
promenna SCHEDULING. Nejlepsi je tuto promennou definovat v Makefile.
